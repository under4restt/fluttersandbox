import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:untitled/components/CardButton.dart';
import 'package:untitled/components/ProductCard.dart';

class CatalogScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: ListView(
              children: List.generate(100, (index) {
                return ProductCard(index);
              }),
            )
        )
    );
  }
}

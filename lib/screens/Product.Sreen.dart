import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:untitled/components/CardButton.dart';

class ProductScreen extends StatelessWidget {
  final int id;

  const ProductScreen(this.id);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            padding: EdgeInsets.all(16.0),
            child: Flex(
                direction: Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      width: double.infinity,
                      height: 400,
                      child: Image(image: AssetImage('assets/bottle.png'))),
                  Text(
                    'Item $id',
                    style: new TextStyle(
                      color: Colors.black,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 16),
                  Text(
                    'Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text ',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    style: new TextStyle(
                      color: Colors.grey,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  CardButtons(),
                  SizedBox(height: 16),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      onPrimary: Colors.white,
                      minimumSize: Size(double.infinity, 40),
                      elevation: 1,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16))),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Go back!',
                        style: new TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        )),
                  ),
                ])),
      ),
    );
  }
}

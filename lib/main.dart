import 'package:flutter/material.dart';
import 'package:untitled/screens/Catalog.Screen.dart';
import 'package:untitled/screens/Product.Sreen.dart';

const homeRoute = '/Catalog';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: homeRoute,
      onGenerateRoute: Router.generateRoute,
    );
  }
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/Catalog':
        return MaterialPageRoute(builder: (_) => CatalogScreen());
      case '/Product':
        var id = settings.arguments as int;
        return MaterialPageRoute(builder: (_) => ProductScreen(id));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')
              ),
            )
        );
    }
  }
}



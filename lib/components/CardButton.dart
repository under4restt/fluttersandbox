import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CardButtons extends StatefulWidget {
  const CardButtons({
    Key? key,
  }) : super(key: key);

  @override
  _CardButtonsState createState() => _CardButtonsState();
}

class _CardButtonsState extends State<CardButtons> {
  int _count = 1;
  int _price = 100;

  void increment() {
    setState(() {
      _count += 1;
    });
    setPrice();
  }

  void setPrice() {
    setState(() {
      _price = _count * 100;
    });
  }

  void decrement() {
    setState(() {
      _count -= 1;
    });
    setPrice();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              '$_price Rub',
              style: new TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(2, 2),
                      blurRadius: 24,
                    ),
                  ],
                ),
                height: 40,
                width: 140,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: Colors.white,
                          minimumSize: Size(38, 38),
                          elevation: 0,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(16))),
                        ),
                        onPressed: () => increment(),
                        child: Text('+',
                            style: new TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.black,
                            ))),
                    Text('$_count',
                        style: new TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        )),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: Colors.white,
                          minimumSize: Size(38, 38),
                          elevation: 0,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(16))),
                        ),
                        onPressed: () => decrement(),
                        child: Text('-',
                            style: new TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.black,
                            ))),
                  ],
                ))
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:untitled/components/CardButton.dart';

class ProductCard extends StatelessWidget {
  final int id;

  const ProductCard(this.id);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(2, 2),
              blurRadius: 24,
            ),
          ],
        ),
        margin: EdgeInsets.all(16.0),
        height: 180,
        child: ConstrainedBox(
          constraints: BoxConstraints.tightFor(
              width: double.infinity, height: double.infinity),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                onPrimary: Colors.white,
                elevation: 0,
                padding: EdgeInsets.all(16.0),
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16))),
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/Product', arguments: id);
              },
              child: Flex(
                  direction: Axis.horizontal,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image(image: AssetImage('assets/bottle.png')),
                    Flexible(
                      flex: 1,
                      child: new Container(
                          padding: new EdgeInsets.symmetric(horizontal: 8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                'Item $id',
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text Big text ',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              CardButtons(),
                            ],
                          )),
                    )
                  ])),
        ));
  }
}
